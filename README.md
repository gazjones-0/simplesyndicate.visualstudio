# README #

SimpleSyndicate.VisualStudio NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.VisualStudio

### What is this repository for? ###

* Common functionality for working with Visual Studio.

### How do I get started? ###

* See the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.visualstudio/issues?status=new&status=open
