﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using SimpleSyndicate.IO;

namespace SimpleSyndicate.VisualStudio
{
    /// <summary>
    /// Provides easy access to, and manipulation of, a Visual Studio solution.
    /// </summary>
    public class Solution : TextFile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Solution"/> class using the solution at the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to the solution.</param>
        public Solution(string path)
            : base(SolutionHelpers.SolutionFilePath(path))
        {
        }

        /// <summary>
        /// Adds a pre-project solution item with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Name of the item.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public void AddPreProjectSolutionItem(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            Load();
            var startingLine = FindSolutionItemsPreProjectSection();
            while (Lines[startingLine].IndexOf("EndProjectSection", StringComparison.OrdinalIgnoreCase) < 0)
            {
                if (Lines[startingLine].IndexOf(name + " = " + name, StringComparison.OrdinalIgnoreCase) > 0)
                {
                    return;
                }

                startingLine++;
            }

            Lines.Insert(startingLine, "\t\t" + name + " = " + name);
            Save();
        }

        /// <summary>
        /// Returns the line the project with the specified <paramref name="guid"/> starts at, or -1 if it can't be found.
        /// </summary>
        /// <param name="guid">Project to find.</param>
        /// <returns>Line project starts at, or -1 if it can't be found.</returns>
        private int FindProject(string guid)
        {
            var index = 0;
            while (index < Lines.Count)
            {
                if (Lines[index].IndexOf("Project(\"{" + guid + "}\")", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return index;
                }

                index++;
            }

            return -1;
        }

        /// <summary>
        /// Returns the line after the last project in the solution file; if there are no projects, returns the line the
        /// Global section starts on.
        /// </summary>
        /// <returns>Line after the last project in the solution file, or the line the Global section starts if there are no projects.</returns>
        /// <exception cref="InvalidOperationException">Thrown when there are no projects and no Global section in the solution file.</exception>
        private int FindLastProject()
        {
            // work backwards from the last line looking for the end of a project
            var index = Lines.Count - 1;
            while (index >= 0)
            {
                if (Lines[index].IndexOf("EndProject", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return index + 1;
                }

                index--;
            }

            // no projects, so instead look for when the Global section starts as the projects go immediately before this
            index = Lines.Count - 1;
            while (index >= 0)
            {
                if (Lines[index].StartsWith("Global", System.StringComparison.OrdinalIgnoreCase))
                {
                    return index;
                }

                index--;
            }

            // give up
            throw new InvalidOperationException("Can't find any projects or a Global section in Visual Studio solution file.");
        }

        /// <summary>
        /// Returns the line the project with the specified <paramref name="guid"/> starts at; if it doesn't exist it will be added.
        /// </summary>
        /// <param name="guid">Project to find, or add if it doesn't exist.</param>
        /// <param name="name">Name of the project.</param>
        /// <returns>Line project starts at.</returns>
        private int FindOrAddProject(string guid, string name)
        {
            var index = FindProject(guid);
            if (index >= 0)
            {
                return index;
            }

            index = FindLastProject();
            Lines.Insert(index, "EndProject");
            Lines.Insert(index, "Project(\"{" + guid + "}\") = \"" + name + "\", \"" + name + "\", \"{" + Guid.NewGuid().ToString().ToUpperInvariant() + "}\"");
            return index;
        }

        /// <summary>
        /// Returns the line the <c>Solution Items</c> project starts at; if it doesn't exist it will be added.
        /// </summary>
        /// <returns>Line the <c>Solution Items</c> project starts at.</returns>
        private int FindSolutionItemsProject() => FindOrAddProject("2150E333-8FDC-42A3-9474-1A3956D46DE8", "Solution Items");

        /// <summary>
        /// Returns the line the project section with the specified <paramref name="name"/> starts at, or -1 if it can't be found.
        /// </summary>
        /// <param name="startingLine">Line to start searching at.</param>
        /// <param name="name">Name of the project section to find.</param>
        /// <returns>Line the project section starts at, or -1 if it can't be found.</returns>
        private int FindProjectSection(int startingLine, string name)
        {
            while (startingLine < Lines.Count)
            {
                if (Lines[startingLine].IndexOf(" = " + name, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return startingLine;
                }

                if (Lines[startingLine].IndexOf("EndProject", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return -1;
                }

                startingLine++;
            }

            return -1;
        }

        /// <summary>
        /// Returns the line after the last project section.
        /// </summary>
        /// <param name="startingLine">Line to start searching at.</param>
        /// <returns>Line after the last project section.</returns>
        /// <exception cref="InvalidOperationException">Thrown when the end of the project can't be found.</exception>
        private int FindLastProjectSection(int startingLine)
        {
            while (startingLine < Lines.Count)
            {
                if (Lines[startingLine].IndexOf("EndProject", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return startingLine;
                }

                startingLine++;
            }

            throw new InvalidOperationException("Can't find EndProject in Visual Studio solution file.");
        }

        /// <summary>
        /// Returns the line the project section with the specified <paramref name="sectionName"/> starts at; if it doesn't exist it will be added.
        /// </summary>
        /// <param name="startingLine">Line to start searching at.</param>
        /// <param name="sectionName">Name of the project section to find.</param>
        /// <param name="projectName">Name of the project.</param>
        /// <returns>Line the project section starts at.</returns>
        private int FindOrAddProjectSection(int startingLine, string sectionName, string projectName)
        {
            var sectionIndex = FindProjectSection(startingLine, sectionName);
            if (sectionIndex >= 0)
            {
                return sectionIndex;
            }

            sectionIndex = FindLastProjectSection(startingLine);
            Lines.Insert(sectionIndex, "\tEndProjectSection");
            Lines.Insert(sectionIndex, "\tProjectSection(" + projectName + ") = " + sectionName);
            return sectionIndex;
        }

        /// <summary>
        /// Returns the line the <c>Solution Items</c> pre-project section starts at; if it doesn't exist it will be added.
        /// </summary>
        /// <returns>Line the <c>Solution Items</c> pre-project section starts at.</returns>
        private int FindSolutionItemsPreProjectSection() => FindOrAddProjectSection(FindSolutionItemsProject(), "preProject", "SolutionItems");
    }
}
