﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.IO;
using SimpleSyndicate.IO;

namespace SimpleSyndicate.VisualStudio
{
    /// <summary>
    /// Helper methods for solutions.
    /// </summary>
    public static class ProjectHelpers
    {
        /// <summary>
        /// Returns whether a project exists in the specified <paramref name="path"/>; if the <paramref name="path"/>
        /// is a project file <c>true</c> is returned; if the <paramref name="path"/> is a directory, and it contains a project file
        /// <c>true</c> is returned; if the <paramref name="path"/> doesn't contain a project, but does contain a solution, and a
        /// project is found in the sub-directories <c>true</c> is returned; if the <paramref name="path"/> specifies a file that isn't a project
        /// file, it is treated as a directory to search.
        /// </summary>
        /// <param name="path">Path to project file.</param>
        /// <returns><c>true</c> if a project exists; <c>false</c> otherwise.</returns>
        public static bool ProjectExists(string path)
        {
            var project = ProjectFilePathInternal(path, false);
            if (project != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the project file path for the project found in the specified <paramref name="path"/>; if the <paramref name="path"/>
        /// is a project file, this is the one used; if the <paramref name="path"/> is a directory, the first project file found in the
        /// directory is the one used; if the <paramref name="path"/> doesn't contain a project, but does contain a solution, the first
        /// project found in the sub-directories is the one used; if the <paramref name="path"/> specifies a file that isn't a project
        /// file, it is treated as a directory to search.
        /// </summary>
        /// <param name="path">Path to project file.</param>
        /// <returns>The full path of the found project file.</returns>
        /// <exception cref="FileNotFoundException">Thrown when no project file can be found.</exception>
        public static string ProjectFilePath(string path)
        {
            var project = ProjectFilePathInternal(path, false);
            if (project != null)
            {
                return project;
            }

            throw new FileNotFoundException("Couldn't find Visual Studio project file from " + path.ValueOrNullStringIfNull());
        }

        /// <summary>
        /// Returns whether a tests project exists in the specified <paramref name="path"/>; if the <paramref name="path"/>
        /// is a project file <c>true</c> is returned; if the <paramref name="path"/> is a directory, and it contains a project file
        /// <c>true</c> is returned; if the <paramref name="path"/> doesn't contain a project, but does contain a solution, and a
        /// project is found in the sub-directories <c>true</c> is returned; if the <paramref name="path"/> specifies a file that isn't a project
        /// file, it is treated as a directory to search.
        /// </summary>
        /// <param name="path">Path to tests project file.</param>
        /// <returns><c>true</c> if a tests project exists; <c>false</c> otherwise.</returns>
        public static bool TestsProjectExists(string path)
        {
            var project = ProjectFilePathInternal(path, true);
            if (project != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the tests project file path for the project found in the specified <paramref name="path"/>; if the <paramref name="path"/>
        /// is a project file, this is the one used; if the <paramref name="path"/> is a directory, the first project file found in the
        /// directory is the one used; if the <paramref name="path"/> doesn't contain a project, but does contain a solution, the first
        /// project found in the sub-directories is the one used; if the <paramref name="path"/> specifies a file that isn't a project
        /// file, it is treated as a directory to search.
        /// </summary>
        /// <param name="path">Path to tests project file.</param>
        /// <returns>The full path of the found tests project file.</returns>
        /// <exception cref="FileNotFoundException">Thrown when no tests project file can be found.</exception>
        public static string TestsProjectFilePath(string path)
        {
            var project = ProjectFilePathInternal(path, true);
            if (project != null)
            {
                return project;
            }

            throw new FileNotFoundException("Couldn't find Visual Studio tests project file from " + path.ValueOrNullStringIfNull());
        }

        /// <summary>
        /// Returns the project file path for the project found in the specified <paramref name="path"/>; if the <paramref name="path"/>
        /// is a project file, this is the one used; if the <paramref name="path"/> is a directory, the first project file found in the
        /// directory is the one used; if the <paramref name="path"/> doesn't contain a project, but does contain a solution, the first
        /// project found in the sub-directories is the one used; if the <paramref name="path"/> specifies a file that isn't a project
        /// file, it is treated as a directory to search.
        /// </summary>
        /// <param name="path">Path to project file.</param>
        /// <param name="testsProject"><c>true</c> to search for tests project; <c>false</c> otherwise.</param>
        /// <returns>The full path of the found project file, or <c>null</c> if it can't be found.</returns>
        private static string ProjectFilePathInternal(string path, bool testsProject)
        {
            var pathToSearch = path;
            var endsWith = testsProject == true ? ".Tests.csproj" : ".csproj";
            if (File.Exists(pathToSearch))
            {
                // path is a file; if it's a project file we'll use it directly
                if (new FileInfo(pathToSearch).Name.EndsWith(endsWith, StringComparison.OrdinalIgnoreCase))
                {
                    return Path.GetFullPath(new FileInfo(pathToSearch).FullName);
                }

                // not a project file, so search in the directory the file is in
                pathToSearch = new FileInfo(pathToSearch).Directory.FullName;
            }

            // either a file that doesn't exist, or a directory, so see if it's a directory
            if (Directory.Exists(pathToSearch))
            {
                // must be a directory, so see if there's a project file in it
                foreach (var file in Directory.EnumerateFiles(pathToSearch))
                {
                    if (file.EndsWith(endsWith, System.StringComparison.OrdinalIgnoreCase))
                    {
                        return Path.GetFullPath(file);
                    }
                }

                // if there's a solution file, check the sub-directories, we might have been given a path to a solution
                if (DirectoryHelpers.ContainsFileWithNameThatEndsWith(pathToSearch, ".sln"))
                {
                    foreach (var directory in Directory.EnumerateDirectories(pathToSearch))
                    {
                        foreach (var file in Directory.EnumerateFiles(directory))
                        {
                            if (file.EndsWith(endsWith, System.StringComparison.OrdinalIgnoreCase))
                            {
                                return Path.GetFullPath(file);
                            }
                        }
                    }
                }
            }

            // give up
            return null;
        }
    }
}
