﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.IO;

namespace SimpleSyndicate.VisualStudio
{
    /// <summary>
    /// Helper methods for solutions.
    /// </summary>
    public static class SolutionHelpers
    {
        /// <summary>
        /// Returns whether a solution exists in the specified <paramref name="path"/>; if the <paramref name="path"/>
        /// is a solution file, this will be <c>true</c>; if the <paramref name="path"/> is a directory, and it contains a solution
        /// file then <c>true</c> is returned; if the <paramref name="path"/> is a directory with no solution file, but the parent
        /// directory contains a solution file then <c>true</c> is returned; if the <paramref name="path"/> specifies a file that
        /// isn't a solution file, it is treated as a directory to search.
        /// </summary>
        /// <param name="path">Path to solution file.</param>
        /// <returns><c>true</c> if a solution file is found; <c>false</c> otherwise.</returns>
        public static bool SolutionExists(string path)
        {
            var solution = SolutionFilePathInternal(path);
            if (solution != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the solution file path for the solution found in the specified <paramref name="path"/>; if the <paramref name="path"/>
        /// is a solution file, this is the one used; if the <paramref name="path"/> is a directory, the first solution file found in the
        /// directory is the one used; if the <paramref name="path"/> is a directory with no solution file, the first solution file found
        /// in the parent directory is used; if the <paramref name="path"/> specifies a file that isn't a solution file, it is treated as
        /// a directory to search.
        /// </summary>
        /// <param name="path">Path to solution file.</param>
        /// <returns>The full path of the found solution file.</returns>
        /// <exception cref="FileNotFoundException">Thrown when no solution file can be found.</exception>
        public static string SolutionFilePath(string path)
        {
            var solution = SolutionFilePathInternal(path);
            if (solution != null)
            {
                return solution;
            }

            throw new FileNotFoundException("Couldn't find Visual Studio tests project file from " + path.ValueOrNullStringIfNull());
        }

        /// <summary>
        /// Returns the solution file path for the solution found in the specified <paramref name="path"/>; if the <paramref name="path"/>
        /// is a solution file, this is the one used; if the <paramref name="path"/> is a directory, the first solution file found in the
        /// directory is the one used; if the <paramref name="path"/> is a directory with no solution file, the first solution file found
        /// in the parent directory is used; if the <paramref name="path"/> specifies a file that isn't a solution file, it is treated as
        /// a directory to search.
        /// </summary>
        /// <param name="path">Path to solution file.</param>
        /// <returns>The full path of the found solution file, or <c>null</c> if it can't be found.</returns>
        private static string SolutionFilePathInternal(string path)
        {
            var pathToSearch = path;
            if (File.Exists(pathToSearch))
            {
                // path is a file; if it's a solution file we'll use it directly
                if (new FileInfo(pathToSearch).Name.EndsWith(".sln", StringComparison.OrdinalIgnoreCase))
                {
                    return Path.GetFullPath(new FileInfo(pathToSearch).FullName);
                }

                // not a solution file, so search in the directory the file is in
                pathToSearch = new FileInfo(pathToSearch).Directory.FullName;
            }

            // either a file that doesn't exist, or a directory, so see if it's a directory
            if (Directory.Exists(pathToSearch))
            {
                // must be a directory, so see if there's a solution file in it
                foreach (var file in Directory.EnumerateFiles(pathToSearch))
                {
                    if (file.EndsWith(".sln", System.StringComparison.OrdinalIgnoreCase))
                    {
                        return Path.GetFullPath(file);
                    }
                }

                // check the parent directory, we might have been given a path to a project
                foreach (var file in Directory.EnumerateFiles(Directory.GetParent(pathToSearch).FullName))
                {
                    if (file.EndsWith(".sln", System.StringComparison.OrdinalIgnoreCase))
                    {
                        return Path.GetFullPath(file);
                    }
                }
            }

            // give up
            return null;
        }
    }
}
