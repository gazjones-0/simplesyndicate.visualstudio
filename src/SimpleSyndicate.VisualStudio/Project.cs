﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using SimpleSyndicate.IO;

namespace SimpleSyndicate.VisualStudio
{
    /// <summary>
    /// Provides easy access to, and manipulation of, a Visual Studio project.
    /// </summary>
    public class Project : TextFile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Project"/> class using the project at the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to the project.</param>
        public Project(string path)
            : base(ProjectHelpers.ProjectFilePath(path))
        {
        }

        /// <summary>
        /// Gets the name of the project.
        /// </summary>
        /// <value>Project name.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the Visual Studio solution this project is part of.
        /// </summary>
        /// <value>Visual Studio solution this project is part of.</value>
        public Solution Solution { get; private set; }

        /// <summary>
        /// Adds an <c>import</c> for the specified <paramref name="projectPath"/>.
        /// </summary>
        /// <param name="projectPath">Project to import.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="projectPath"/> is <c>null</c>.</exception>
        public void AddProjectImport(string projectPath)
        {
            if (projectPath == null)
            {
                throw new ArgumentNullException(nameof(projectPath));
            }

            AddImport("Project", projectPath);
        }

        /// <summary>
        /// Adds a content item with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Item name.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public void AddItem(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            AddItem("None", "Content", name, null, null);
        }

        /// <summary>
        /// Adds a content item with the specified <paramref name="name"/> and <paramref name="subType"/>.
        /// </summary>
        /// <param name="name">Item name.</param>
        /// <param name="subType">Item sub-type.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> or <paramref name="subType"/> are <c>null</c>.</exception>
        public void AddItem(string name, string subType)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (subType == null)
            {
                throw new ArgumentNullException(nameof(subType));
            }

            AddItem("None", "Content", name, subType, null);
        }

        /// <summary>
        /// Adds a content item with the specified <paramref name="name"/>, <paramref name="subType"/> and what it's <paramref name="dependentUpon"/>.
        /// </summary>
        /// <param name="name">Item name.</param>
        /// <param name="subType">Item sub-type.</param>
        /// <param name="dependentUpon">Anything the item is dependent upon.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/>, <paramref name="subType"/> or <paramref name="dependentUpon"/> are <c>null</c>.</exception>
        public void AddItem(string name, string subType, string dependentUpon)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (subType == null)
            {
                throw new ArgumentNullException(nameof(subType));
            }

            if (dependentUpon == null)
            {
                throw new ArgumentNullException(nameof(dependentUpon));
            }

            AddItem("None", "Content", name, subType, dependentUpon);
        }

        /// <summary>
        /// Adds a code analysis dictionary item with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Item name.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public void AddCodeAnalysisDictionaryItem(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            AddItem("CodeAnalysisDictionary", null, name, null, null);
        }

        /// <summary>
        /// Adds a compile item with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Item name.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public void AddCompileItem(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            AddItem("Compile", null, name, null, null);
        }

        /// <summary>
        /// Adds a content item with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Item name.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public void AddContentItem(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            AddItem("None", "Content", name, null, null);
        }

        /// <summary>
        /// Adds a global property with the specified <paramref name="name"/> and <paramref name="value"/>.
        /// </summary>
        /// <param name="name">Property name.</param>
        /// <param name="value">Property value.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> or <paramref name="value"/> are <c>null</c>.</exception>
        public void AddGlobalProperty(string name, string value)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            AddProperty(null, name, value);
        }

        /// <summary>
        /// Adds a debug property with the specified <paramref name="name"/> and <paramref name="value"/>.
        /// </summary>
        /// <param name="name">Property name.</param>
        /// <param name="value">Property value.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> or <paramref name="value"/> are <c>null</c>.</exception>
        public void AddDebugProperty(string name, string value)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            AddProperty("Debug", name, value);
        }

        /// <summary>
        /// Adds a release property with the specified <paramref name="name"/> and <paramref name="value"/>.
        /// </summary>
        /// <param name="name">Property name.</param>
        /// <param name="value">Property value.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> or <paramref name="value"/> are <c>null</c>.</exception>
        public void AddReleaseProperty(string name, string value)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            AddProperty("Release", name, value);
        }

        /// <summary>
        /// Adds a reference to the project.
        /// </summary>
        /// <param name="include">Reference to include.</param>
        /// <param name="hintPath">Hint path.</param>
        /// <param name="isPrivate">Whether the reference is private.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="include"/> or <paramref name="hintPath"/> are <c>null</c>.</exception>
        public void AddReference(string include, string hintPath, bool isPrivate)
        {
            if (include == null)
            {
                throw new ArgumentNullException(nameof(include));
            }

            if (hintPath == null)
            {
                throw new ArgumentNullException(nameof(hintPath));
            }

            AddItem(include, hintPath, isPrivate);
        }

        /// <summary>
        /// Returns the global property with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Property name.</param>
        /// <returns>Property value, or <c>null</c> if it cannot be found.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public string GetGlobalProperty(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetProperty(null, name);
        }

        /// <summary>
        /// Returns the debug property with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Property name.</param>
        /// <returns>Property value, or <c>null</c> if it cannot be found.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public string GetDebugProperty(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetProperty("Debug", name);
        }

        /// <summary>
        /// Returns the release property with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Property name.</param>
        /// <returns>Property value, or <c>null</c> if it cannot be found.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public string GetReleaseProperty(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetProperty("Release", name);
        }

        /// <summary>
        /// Removes the compile item with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Item name.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="name"/> is <c>null</c>.</exception>
        public void RemoveCompileItem(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            RemoveItem("Compile", name);
        }

        /// <summary>
        /// Loads the project file.
        /// </summary>
        protected override void OnLoad()
        {
            Name = System.IO.Path.GetFileNameWithoutExtension(FullName);
            Solution = new Solution(FullName);
        }

        /// <summary>
        /// Adds an <c>import</c> for the specified <paramref name="type"/> and <paramref name="value"/>.
        /// </summary>
        /// <param name="type">Import type.</param>
        /// <param name="value">Import value.</param>
        private void AddImport(string type, string value)
        {
            Load();
            var index = 0;
            while (index < Lines.Count)
            {
                if (Lines[index].IndexOf("<Import " + type + "=\"" + value + "\"", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return;
                }

                if (Lines[index].IndexOf("</Project>", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    Lines.Insert(index, "  <Import " + type + "=\"" + value + "\" />");
                    Save();
                    return;
                }

                index++;
            }
        }

        /// <summary>
        /// Adds an item of the specified <paramref name="type"/> with the specified details.
        /// </summary>
        /// <param name="type">Item type.</param>
        /// <param name="alternativeType">Alternative type.</param>
        /// <param name="name">Item name.</param>
        /// <param name="subType">Item sub-type.</param>
        /// <param name="dependentUpon">Anything the item is dependent upon.</param>
        private void AddItem(string type, string alternativeType, string name, string subType, string dependentUpon)
        {
            Load();
            var index = FindOrAddItemGroup(type, alternativeType);
            while (Lines[index].IndexOf("</ItemGroup>", StringComparison.OrdinalIgnoreCase) < 0)
            {
                if (Lines[index].IndexOf("Include=\"" + name + "\"", StringComparison.OrdinalIgnoreCase) > 0)
                {
                    Lines[index] = Lines[index].Replace("<None ", "<" + type + " ");
                    Lines[index] = Lines[index].Replace("<Content ", "<" + type + " ");
                    index++;
                    if (Lines[index].IndexOf("<SubType>", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        index--;
                        Lines[index] = Lines[index].Replace(">", " />");
                        index++;
                        Lines.RemoveAt(index);
                        while (Lines[index].IndexOf("</None>", StringComparison.OrdinalIgnoreCase) < 0)
                        {
                            Lines.RemoveAt(index);
                        }

                        Lines.RemoveAt(index);
                    }

                    Save();
                    return;
                }

                index++;
            }

            if (string.IsNullOrWhiteSpace(subType) && string.IsNullOrWhiteSpace(dependentUpon))
            {
                Lines.Insert(index, "    <" + type + " Include=\"" + name + "\" />");
            }
            else
            {
                Lines.Insert(index, "    </" + type + ">");
                if (!string.IsNullOrWhiteSpace(subType))
                {
                    Lines.Insert(index, "      <SubType>" + subType + "</SubType>");
                }

                if (!string.IsNullOrWhiteSpace(dependentUpon))
                {
                    Lines.Insert(index, "      <DependentUpon>" + dependentUpon + "</DependentUpon>");
                }

                Lines.Insert(index, "    <" + type + " Include=\"" + name + "\">");
            }

            Save();
        }

        /// <summary>
        /// Adds a reference item to the project.
        /// </summary>
        /// <param name="include">Reference to include.</param>
        /// <param name="hintPath">Hint path.</param>
        /// <param name="isPrivate">Whether the reference is private.</param>
        private void AddItem(string include, string hintPath, bool? isPrivate)
        {
            Load();
            var index = FindOrAddItemGroup("Reference", null);
            while (Lines[index].IndexOf("</ItemGroup>", StringComparison.OrdinalIgnoreCase) < 0)
            {
                if (Lines[index].IndexOf("Include=\"" + include + "\"", StringComparison.OrdinalIgnoreCase) > 0)
                {
                    return;
                }

                index++;
            }

            if (string.IsNullOrWhiteSpace(hintPath) && isPrivate == null)
            {
                Lines.Insert(index, "    <Reference Include=\"" + include + "\" />");
            }
            else
            {
                Lines.Insert(index, "    </Reference>");
                if (isPrivate != null)
                {
                    Lines.Insert(index, "      <Private>" + isPrivate.ToString() + "</Private>");
                }

                if (!string.IsNullOrWhiteSpace(hintPath))
                {
                    Lines.Insert(index, "      <HintPath>" + hintPath + "</HintPath>");
                }

                Lines.Insert(index, "    <Reference Include=\"" + include + "\">");
            }

            Save();
        }

        /// <summary>
        /// Adds a property with the specified <paramref name="name"/> and <paramref name="value"/> to the property group of the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="type">Group type to add property to.</param>
        /// <param name="name">Property name.</param>
        /// <param name="value">Property value.</param>
        private void AddProperty(string type, string name, string value)
        {
            Load();
            var index = FindPropertyGroup(type, 0);
            while (index >= 0)
            {
                var existingProperty = false;
                while (Lines[index].IndexOf("</PropertyGroup>", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    var start = Lines[index].IndexOf("<" + name + ">", StringComparison.OrdinalIgnoreCase);
                    if (start > 0)
                    {
                        var end = Lines[index].IndexOf("</" + name + ">", start, StringComparison.OrdinalIgnoreCase);
                        if (end > 0)
                        {
                            Lines[index] = Lines[index].Substring(0, start) + "<" + name + ">" + value + Lines[index].Substring(end);
                        }

                        existingProperty = true;
                    }

                    index++;
                }

                if (existingProperty == false)
                {
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        Lines.Insert(index, "    <" + name + " />");
                    }
                    else
                    {
                        Lines.Insert(index, "    <" + name + ">" + value + "</" + name + ">");
                    }
                }

                index = FindPropertyGroup(type, index + 1);
            }

            Save();
        }

        /// <summary>
        /// Returns a property with the specified <paramref name="name"/> from the property group of the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="type">Group type to get property from.</param>
        /// <param name="name">Property name.</param>
        /// <returns>Property value, or <c>null</c> if it can't be found.</returns>
        private string GetProperty(string type, string name)
        {
            Load();
            var index = FindPropertyGroup(type, 0);
            while (index >= 0)
            {
                while (Lines[index].IndexOf("</PropertyGroup>", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    var start = Lines[index].IndexOf("<" + name + ">", StringComparison.OrdinalIgnoreCase);
                    if (start > 0)
                    {
                        var end = Lines[index].IndexOf("</" + name + ">", start, StringComparison.OrdinalIgnoreCase);
                        if (end > 0)
                        {
                            return Lines[index].Substring(start + ("<" + name + ">").Length, end - start - ("<" + name + ">").Length);
                        }
                    }

                    index++;
                }

                index = FindPropertyGroup(type, index + 1);
            }

            Save();

            return null;
        }

        /// <summary>
        /// Returns the line the item group containing the specified <paramref name="typeToFind"/> or <paramref name="alternativeTypeToFind"/>
        /// starts at, or -1 if it can't be found.
        /// </summary>
        /// <param name="typeToFind">Type to find.</param>
        /// <param name="alternativeTypeToFind">Alternative type</param>
        /// <returns>Line the item group containing the specified <paramref name="typeToFind"/> or <paramref name="alternativeTypeToFind"/> starts at, or -1 if it can't be found.</returns>
        private int FindItemGroup(string typeToFind, string alternativeTypeToFind)
        {
            var index = 0;
            while (index < Lines.Count)
            {
                if (Lines[index].IndexOf("<" + typeToFind, System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return index;
                }

                if (!string.IsNullOrWhiteSpace(alternativeTypeToFind))
                {
                    if (Lines[index].IndexOf("<" + alternativeTypeToFind, System.StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return index;
                    }
                }

                index++;
            }

            return -1;
        }

        /// <summary>
        /// Returns the line after the last item group.
        /// </summary>
        /// <returns>The line after the last item group.</returns>
        private int FindLastItemGroup()
        {
            var index = Lines.Count - 1;
            while (index >= 0)
            {
                if (
                    (Lines[index].IndexOf("</ItemGroup>", System.StringComparison.OrdinalIgnoreCase) >= 0)
                    || (Lines[index].IndexOf("<ItemGroup />", System.StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    if (index < (Lines.Count - 1))
                    {
                        if (!Lines[index + 1].Trim().StartsWith("</", StringComparison.OrdinalIgnoreCase))
                        {
                            return index + 1;
                        }
                    }
                    else
                    {
                        return index + 1;
                    }
                }

                index--;
            }

            throw new InvalidOperationException("Can't find ItemGroup in Visual Studio project file.");
        }

        /// <summary>
        /// Returns the line the item group containing the specified <paramref name="typeToFind"/> or <paramref name="alternativeTypeToFind"/>
        /// starts at; if it doesn't exist it will be added.
        /// </summary>
        /// <param name="typeToFind">Type to find.</param>
        /// <param name="alternativeTypeToFind">Alternative type</param>
        /// <returns>Line the item group containing the specified <paramref name="typeToFind"/> or <paramref name="alternativeTypeToFind"/> starts at.</returns>
        private int FindOrAddItemGroup(string typeToFind, string alternativeTypeToFind)
        {
            var itemGroupIndex = FindItemGroup(typeToFind, alternativeTypeToFind);
            if (itemGroupIndex >= 0)
            {
                return itemGroupIndex;
            }

            itemGroupIndex = FindLastItemGroup();
            Lines.Insert(itemGroupIndex, "  </ItemGroup>");
            Lines.Insert(itemGroupIndex, "  <ItemGroup>");
            return itemGroupIndex;
        }

        /// <summary>
        /// Returns the line the property group of the specified <paramref name="type"/> starts at, or -1 if it can't be found; if
        /// <paramref name="type"/> is <c>null</c> or whitespace the first property group found is returned.
        /// </summary>
        /// <param name="type">Type to find, or <c>null</c> or whitespace to find any type.</param>
        /// <param name="startingLine">Line to start searching at.</param>
        /// <returns>Starting line of the property group found, or -1 if it can't be found.</returns>
        private int FindPropertyGroup(string type, int startingLine)
        {
            while (startingLine < Lines.Count)
            {
                if (string.IsNullOrWhiteSpace(type))
                {
                    if (Lines[startingLine].IndexOf("<PropertyGroup>", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return startingLine;
                    }
                }
                else
                {
                    if ((Lines[startingLine].IndexOf("<PropertyGroup", StringComparison.OrdinalIgnoreCase) >= 0)
                        && (Lines[startingLine].IndexOf("'" + type, StringComparison.OrdinalIgnoreCase) >= 0))
                    {
                        return startingLine;
                    }
                }

                startingLine++;
            }

            return -1;
        }

        /// <summary>
        /// Removes the item of the specified <paramref name="type"/> with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="type">Item type.</param>
        /// <param name="name">Item name.</param>
        private void RemoveItem(string type, string name)
        {
            Load();
            var index = 0;
            while (index < Lines.Count)
            {
                if (Lines[index].IndexOf("<" + type + " ", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    if (Lines[index].IndexOf(" Include=\"" + name + "\"", System.StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        Lines.RemoveAt(index);
                        Save();
                        return;
                    }
                }

                index++;
            }
        }
    }
}
